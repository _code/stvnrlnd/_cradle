FROM mojavehq/core:2019.06.22.17.34

LABEL maintainer "Steven Roland <steven@mojave.io>"

WORKDIR /var/www/site

COPY web/composer.lock web/composer.json /var/www/site/
RUN composer install --no-scripts --no-autoloader --ansi --no-interaction

COPY web /var/www/site

RUN chown -R www-data:www-data \
    /var/www/site/storage \
    /var/www/site/bootstrap/cache

VOLUME /var/www/site/storage

RUN php artisan view:clear --quiet \
	&& php artisan cache:clear --quiet \
	&& php artisan config:cache --quiet